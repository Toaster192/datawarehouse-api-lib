"""Datawarehouse Client."""
from restclient import base

from datawarehouse import objects


class KCIDB:
    # pylint: disable=too-few-public-methods
    """KCIDB managers."""

    def __init__(self, api):
        """Initialize."""
        self.data = objects.KCIDBEndpointManager(api)
        self.checkouts = objects.KCIDBCheckoutManager(api)
        self.builds = objects.KCIDBBuildManager(api)
        self.tests = objects.KCIDBTestManager(api)
        self.submit = objects.KCIDBSubmitManager(api)


class Datawarehouse:
    # pylint: disable=too-few-public-methods
    """Datawarehouse client."""

    def __init__(self, host, *args, **kwargs):
        """Initialize."""
        api = base.APIManager(host, *args, **kwargs)
        self.pipeline = objects.PipelineManager(api)
        self.issue = objects.IssueManager(api)
        self.issue_regex = objects.IssueRegexManager(api)
        self.test = objects.TestManager(api)
        self.kcidb = KCIDB(api)
