"""API Objects."""
from restclient.base import CREATEMethod
from restclient.base import GETMethod
from restclient.base import LISTMethod
from restclient.base import ObjectDELETEMethod
from restclient.base import RESTManager
from restclient.base import RESTObject


class Maintainer(RESTObject):
    """Maintainer object."""


class File(RESTObject):
    """File object."""


class TestStat(RESTObject):
    """TestStat object."""


class TestStatManager(RESTManager, GETMethod):
    """TestStat manager."""

    _obj_cls = TestStat
    _path = 'api/1/test/{test_id}/stats'
    _from_parent_attrs = ('test_id:id', )


class Test(RESTObject):
    """Test object."""

    _managers = {
        'stats': 'TestStatManager',
    }


class TestManager(RESTManager, GETMethod, LISTMethod):
    """Test manager."""

    _obj_cls = Test
    _path = 'api/1/test'


class TestRun(RESTObject):
    """TestRun object."""


class TestRunManager(RESTManager, LISTMethod):
    """TestRun manager."""

    _obj_cls = TestRun
    _path = 'api/1/pipeline/{pipeline_id}/jobs/test'
    _from_parent_attrs = ('pipeline_id', )


class Patch(RESTObject):
    """Patch object."""


class Report(RESTObject):
    """Report object."""


class IssueOccurrence(RESTObject, ObjectDELETEMethod):
    """IssueOccurrence object."""


class Action(RESTObject):
    """Action object."""


class Issue(RESTObject):
    """Issue object."""

    _managers = {
    }


class IssueManager(RESTManager, GETMethod, LISTMethod):
    """Issue manager."""

    _obj_cls = Issue

    _path = 'api/1/issue'


class Pipeline(RESTObject):
    """Pipeline object."""

    _id = 'pipeline_id'
    _managers = {}


class PipelineManager(RESTManager, GETMethod):
    """Pipeline manager."""

    _obj_cls = Pipeline
    _path = 'api/1/pipeline'


class IssueRegex(RESTObject):
    """IssueRegex object."""


class IssueRegexManager(RESTManager, LISTMethod):
    """IssueRegex manager."""

    _obj_cls = IssueRegex
    _path = 'api/1/issue/-/regex'


class KCIDBData(RESTObject):
    """KCIDBData object."""


class KCIDBEndpointManager(RESTManager, GETMethod):
    """KCIDB data manager."""

    _obj_cls = KCIDBData
    _path = 'api/1/kcidb/data/{name}'


class KCIDBCheckout(RESTObject):
    """KCIDBCheckout object."""

    _managers = {
        'action_triaged': 'KCIDBCheckoutActionTriagedManager',
        'builds': 'KCIDBCheckoutBuildManager',
        'issues': 'KCIDBCheckoutIssueManager',
        'reports': 'KCIDBCheckoutReportManager',
    }


class KCIDBCheckoutManager(RESTManager, GETMethod, LISTMethod):
    """KCIDBCheckout manager."""

    _obj_cls = KCIDBCheckout
    _path = 'api/1/kcidb/checkouts'


class KCIDBBuild(RESTObject):
    """KCIDBBuild object."""

    _managers = {
        'action_triaged': 'KCIDBBuildActionTriagedManager',
        'tests': 'KCIDBBuildTestManager',
        'issues': 'KCIDBBuildIssueManager',
    }


class KCIDBBuildManager(RESTManager, GETMethod):
    """KCIDBBuild manager."""

    _obj_cls = KCIDBBuild
    _path = 'api/1/kcidb/builds'


class KCIDBCheckoutBuildManager(RESTManager, LISTMethod):
    """KCIDBCheckout build manager."""

    _obj_cls = KCIDBBuild
    _from_parent_attrs = ('checkout_iid:misc.iid',)
    _path = 'api/1/kcidb/checkouts/{checkout_iid}/builds'


class KCIDBTest(RESTObject):
    """KCIDBTest object."""

    _managers = {
        'action_triaged': 'KCIDBTestActionTriagedManager',
        'issues': 'KCIDBTestIssueManager',
    }


class KCIDBTestManager(RESTManager, GETMethod):
    """KCIDBTest manager."""

    _obj_cls = KCIDBTest
    _path = 'api/1/kcidb/tests'


class KCIDBBuildTestManager(RESTManager, LISTMethod):
    """KCIDBBuild test manager."""

    _obj_cls = KCIDBTest
    _from_parent_attrs = ('build_iid:misc.iid',)
    _path = 'api/1/kcidb/builds/{build_iid}/tests'


class KCIDBSubmitManager(RESTManager, CREATEMethod):
    """KCIDB submit manager."""

    _obj_cls = KCIDBData
    _path = 'api/1/kcidb/submit'


class KCIDBCheckoutIssueManager(RESTManager, GETMethod, LISTMethod, CREATEMethod):
    """KCIDBCheckout issue manager."""

    _obj_cls = Issue
    _from_parent_attrs = ('checkout_iid:misc.iid',)
    _path = 'api/1/kcidb/checkouts/{checkout_iid}/issues'


class KCIDBBuildIssueManager(RESTManager, GETMethod, LISTMethod, CREATEMethod):
    """KCIDBBuild issue manager."""

    _obj_cls = Issue
    _from_parent_attrs = ('build_iid:misc.iid',)
    _path = 'api/1/kcidb/builds/{build_iid}/issues'


class KCIDBTestIssueManager(RESTManager, GETMethod, LISTMethod, CREATEMethod):
    """KCIDBTest issue manager."""

    _obj_cls = Issue
    _from_parent_attrs = ('test_iid:misc.iid',)
    _path = 'api/1/kcidb/tests/{test_iid}/issues'


class KCIDBCheckoutReportManager(RESTManager, GETMethod, LISTMethod, CREATEMethod):
    """KCIDBCheckout report manager."""

    _obj_cls = Report
    _from_parent_attrs = ('checkout_iid:misc.iid',)
    _path = 'api/1/kcidb/checkouts/{checkout_iid}/reports'


class KCIDBCheckoutActionTriagedManager(RESTManager, CREATEMethod):
    """KCIDBCheckout Action Triaged manager."""

    _from_parent_attrs = ('checkout_iid:misc.iid',)
    _path = 'api/1/kcidb/checkouts/{checkout_iid}/actions/triaged'


class KCIDBBuildActionTriagedManager(RESTManager, CREATEMethod):
    """KCIDBBuild Action Triaged manager."""

    _from_parent_attrs = ('build_iid:misc.iid',)
    _path = 'api/1/kcidb/builds/{build_iid}/actions/triaged'


class KCIDBTestActionTriagedManager(RESTManager, CREATEMethod):
    """KCIDBTest Action Triaged manager."""

    _from_parent_attrs = ('test_iid:misc.iid',)
    _path = 'api/1/kcidb/tests/{test_iid}/actions/triaged'
